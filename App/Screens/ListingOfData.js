import React, { Component } from "react";
import { ListView, Text, TouchableHighlight, View } from "react-native";
import styles from "../Styles/Styles";

import { SwipeListView } from "react-native-swipe-list-view";

class Listviewexample extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: ""
    };

    fetch("http://127.0.0.1:3000/getdata")
      .then(response => response.json())
      .then(responseJson => {
        this.ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.setState({
          listType: "FlatList",
          data: JSON.stringify(responseJson),
          listViewData: responseJson.map(responseJson => ({
            key: JSON.stringify(responseJson._id),
            Emp_id: JSON.stringify(responseJson.Emp_id),
            Emp_Name: JSON.stringify(responseJson.Emp_Name),
            Emp_Age: JSON.stringify(responseJson.Emp_age)
          }))
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  closeRow(rowMap, rowKey) {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow();
    }
  }

  deleteRow(rowMap, rowKey) {
    this.closeRow(rowMap, rowKey);
    const newData = [...this.state.listViewData];
    const prevIndex = this.state.listViewData.findIndex(
      item => item.key === rowKey
    );
    newData.splice(prevIndex, 1);
    this.setState({ listViewData: newData });
  }

  deleteSectionRow(rowMap, rowKey) {
    this.closeRow(rowMap, rowKey);
    var [section, row] = rowKey.split(".");
    const newData = [...this.state.sectionListData];
    const prevIndex = this.state.sectionListData[section].data.findIndex(
      item => item.key === rowKey
    );
    newData[section].data.splice(prevIndex, 1);
    this.setState({ sectionListData: newData });
  }

  onRowDidOpen = (rowKey, rowMap) => {
    console.log("This row opened", rowKey);
    setTimeout(() => {
      this.closeRow(rowMap, rowKey);
    }, 2000);
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.listType === "FlatList" && (
          <SwipeListView
            useFlatList
            data={this.state.listViewData}
            renderItem={(data, rowMap) => (
              <TouchableHighlight
                style={styles.rowFront}
                underlayColor={"#AAA"}
              >
                <View style={{ flex: 1, alignItems: "center" }}>
                  <View
                    style={{ flexDirection: "row", alignItems: "flex-start" }}
                  >
                    <Text style={{ fontWeight: "bold", color: "black" }}>
                      Employee ID:{" "}
                    </Text>
                    <Text>{data.item.Emp_id} </Text>
                  </View>

                  <View
                    style={{ flexDirection: "row", alignItems: "flex-start" }}
                  >
                    <Text style={{ fontWeight: "bold", color: "black" }}>
                      Employee Name:{" "}
                    </Text>
                    <Text>{data.item.Emp_Name} </Text>
                  </View>

                  <View
                    style={{ flexDirection: "row", alignItems: "flex-start" }}
                  >
                    <Text style={{ fontWeight: "bold", color: "black" }}>
                      Employee Age:{" "}
                    </Text>
                    <Text>{data.item.Emp_Age} </Text>
                  </View>
                </View>
              </TouchableHighlight>
            )}
            leftOpenValue={75}
            rightOpenValue={-150}
            previewRowKey={"0"}
            previewOpenValue={-40}
            previewOpenDelay={3000}
            onRowDidOpen={this.onRowDidOpen}
          />
        )}
      </View>
    );
  }
}

export default Listviewexample;
