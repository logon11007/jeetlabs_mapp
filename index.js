/** @format */

import { AppRegistry } from "react-native";
import App from "./App";
import { name as appName } from "./app.json";
import Listviewexample from "./App/Screens/ListingOfData";

AppRegistry.registerComponent(appName, () => Listviewexample);
